# Where to download VS Code from

- Find the download options [here](https://code.visualstudio.com/download)

# Plugins we need/used in VS Code

Open the readme in Visual Studio Code and CTRL click / Cmd click the vscode links below to download the extensions. You can also follow the steps in the docs too.

- [Extension Pack for Java](vscode:extension/vscjava.vscode-java-pack). Docs can be found [here](https://code.visualstudio.com/docs/languages/java)
- [Gradle for Java](vscode:extension/vscjava.vscode-gradle). Docs can be found [here](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-gradle)

# We will need local setup of Java, Gradle & Git

- Instructions to download Java, Gradle & Git can be found [here](https://moodle.socs.uoguelph.ca/mod/forum/discuss.php?d=4639#p10274)




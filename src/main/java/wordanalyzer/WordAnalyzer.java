package wordanalyzer;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class WordAnalyzer {
    private String phraseOfWords;

    public WordAnalyzer() {

        phraseOfWords = "Wherever you go, there you are";

    }

    public WordAnalyzer(String words) {

        phraseOfWords = words;

    }

    public void setWords(String wordString) {

        phraseOfWords = wordString;

    }

    public String getWords() {

        return phraseOfWords;

    }

    public int countLetters() {

        int i;
        int letCount = 0;

        for (i = 0; i < phraseOfWords.length(); i++) {

            if (Character.isLetter(phraseOfWords.charAt(i))) {

                letCount++;

            }

        }

        return letCount;
    }

    public int countWords() {

        String[] wordStore;
        int wordCount;

        wordStore = phraseOfWords.split(" ");

        wordCount = wordStore.length;

        return wordCount;

    }

    public int countUniqueLetters() {

        boolean[] uniqueLetters = new boolean[Character.MAX_VALUE];
        int i;
        int uniqueLetterCount = 0;

        for (i = 0; i < phraseOfWords.length(); i++) {

            uniqueLetters[phraseOfWords.charAt(i)] = true;

        }

        for (i = 0; i < uniqueLetters.length; i++) {

            if (uniqueLetters[i]) {

                uniqueLetterCount++;

            }

        }

        return uniqueLetterCount;

    }

    /**
     * Case-sensitive method that checks if words exist.
     * Exercise: make it non-case-sensitive
     * @param target The word to find in the string
     * @return true if the target exists, false otherwise
     */
    public boolean wordExists(String target) {
        return phraseOfWords.contains(target);
    }

    public boolean wordExistsInFile(String filePath, String target) throws IOException {

        if (filePath == null||target == null) {
            return false;
        }

        Path path = FileSystems.getDefault().getPath(filePath);

        List<String> lines = Files.readAllLines(path);
        String joinedLines = String.join("\n", lines);

        setWords(joinedLines);

        return wordExists(target);
    }

}

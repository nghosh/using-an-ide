package wordanalyzer;

public class Runner {
    public static void main(String[] args) {
        WordAnalyzer wordAnalyzer = new WordAnalyzer();

        System.out.println("The number of letters are " + wordAnalyzer.countLetters());
    }
}

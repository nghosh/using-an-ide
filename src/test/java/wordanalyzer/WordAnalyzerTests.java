package wordanalyzer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

/**
 * Agenda:
 *
 * 1. Walking through the markdown template
 * to generate test case table - one table per method
 *
 * 2. How to write a Junit Test case by referring to the table
 *
 * 3. assert true, assert false, assert equals and expect for exceptions.
 */
public class WordAnalyzerTests {
    
    private WordAnalyzer objectToTest;

    @Before
    public void setup() {
        objectToTest = new WordAnalyzer();
    }

    @Test
    public void testDefaultConstructor() {

        // Arrange
        String expectedString = "Wherever you go, there you are";

        // Act

        // We could use objectToTest, but I prefer testing
        // constructors separately to keep the test sanity
        // Constructor testing are a special cases
        WordAnalyzer testConstructor = new WordAnalyzer();

        // Assert
        assertNotNull(testConstructor);
        assertEquals(expectedString, testConstructor.getWords()); // Spy on state
    }

    @Test
    public void testOneArgConstructor() {
        // Arrange
        String expectedString = "Try to philly chicken wrap from Osmows";

        // Act
        WordAnalyzer testConstructor = new WordAnalyzer(expectedString);

        // Assert
        assertNotNull(testConstructor);
        assertEquals(expectedString, testConstructor.getWords()); // Spy on state
    }

    @Test
    public void testSetWords() {
        // Arrange
        String expectedResult = "How's OOP today";

        // Act
        objectToTest.setWords(expectedResult); // Call method to test
        String actualResult = objectToTest.getWords(); // Next, spy on the member variable to test the state

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testGetWords() {
        // Arrange
        String expectedResult = "Chicken on the Rocks";

        // Act
        objectToTest.setWords(expectedResult); // Call method to test
        String actualResult = objectToTest.getWords(); // Next, spy on the member variable to test the state

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testCountLetters() {
        // Arrange
        String inputString = "19 Wyndham St N, Guelph, ON N1H 4E5";
        objectToTest.setWords(inputString);

        // Act
        int actualResult = objectToTest.countLetters();

        // Assert
        assertEquals(21, actualResult);

    }

    /**
     * Multiple whitespaces will fail our code
     * Fix it and write a test case for that
     */
    @Test
    public void testCountWordsSingleSpaces() {
        // Arrange
        String inputString = "Chicken on Sticks";
        objectToTest.setWords(inputString);

        // Act
        int actualResult = objectToTest.countWords();

        // Assert
        assertEquals(3, actualResult);
    }

    @Test
    public void testCountUniqueLetters() {
        // Arrange
        String inputString = "Shawarma";
        objectToTest.setWords(inputString);

        // Act
        int actualResult = objectToTest.countUniqueLetters();

        // Assert
        assertEquals(6, actualResult);
    }

    @Test
    public void testWordExists() {
        // Arrange
        String inputString = "We are making what you are craving";
        objectToTest.setWords(inputString);

        // Act
        boolean actualResult = objectToTest.wordExists("craving");

        // Assert
        assertTrue(actualResult);
    }

    @Test
    public void testNoWordExists() {
        // Arrange
        String inputString = "We are making what you are craving";
        objectToTest.setWords(inputString);

        // Act
        boolean actualResult = objectToTest.wordExists("Hello");

        // Assert
        assertFalse(actualResult);
    }

    @Test
    public void testWordExistsInFileWithFilePresent() throws IOException {

        // Usually hardcoding the filepath is not a good idea
        // We get the classpath from the current context and grab the file name
        // Arrange & Act
        boolean actual = objectToTest.wordExistsInFile("src/test/resources/osmows-slogan.txt", "craving");

        // Assert
        assertTrue(actual);
    }

    @Test(expected = NoSuchFileException.class)
    public void testWordExistsInFileWithFileAbsent() throws IOException {
        // Arrange & Act
        objectToTest.wordExistsInFile("src/test/resources/hello.txt", "Osmows");

        // Assertion is done inside the expect statement
    }

    @Test
    public void testWordExistsInFileWithNullArgs() throws IOException {
        // Arrange & Act
        boolean actual = objectToTest.wordExistsInFile(null, null);

        // Assert
        assertFalse(actual);
    }

    /**
     * Second way of testing exceptions
     */
    @Test
    public void testWordExistsInFile() {
        // Arrange & Act
        NoSuchFileException exception = assertThrows(NoSuchFileException.class, 
                () -> objectToTest.wordExistsInFile("src/test/resources/hello.txt", "Osmows"));

        // Assert
        assertEquals("src/test/resources/hello.txt", exception.getMessage());
    }
}
